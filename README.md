# Europeana harvesting docker image

Extension of the
[CLARIN OAI-PMH harvester](https://gitlab.com/CLARIN-ERIC/docker-oai-harvester)
adapted to harvest resources from [Europeana](https://www.europeana.eu) with
an added 'preflight' stage for content filtering using the Europeana APIs.

For more information and usage instructions, see 
[compose_europeana_oai](https://gitlab.com/CLARIN-ERIC/compose_europeana_oai/).