#!/bin/bash
set -e
BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
LOGFILE='/app/europeana-harvest.log'

echo "$(date) Filtering harvesting configuration" | tee -a "$LOGFILE"
(cd "${BASE_DIR}" && \
	bash ./filter-oai-config.sh) >> "$LOGFILE"

if [ "${SKIP_PREFLIGHT}" == "true" ]; then
	echo "$(date) SKIPPING preflight (SKIP_PREFLIGHT=${SKIP_PREFLIGHT})" | tee -a "$LOGFILE"
else
	echo "$(date) Starting preflight (SKIP_PREFLIGHT=${SKIP_PREFLIGHT})" | tee -a "$LOGFILE"
	(cd "${BASE_DIR}" && \
		bash ./europeana-preflight.sh) > '/app/oai/resources/assets/selection.xml' 2>> "$LOGFILE"
fi

echo "$(date) Starting harvest" | tee -a "$LOGFILE"
/app/oai/run.sh -n europeana -o europeana >> "$LOGFILE"

echo "$(date) Post-processing harvesting output" | tee -a "$LOGFILE"
(cd "${BASE_DIR}" && \
	bash ./post-harvest.sh) >> "$LOGFILE"
