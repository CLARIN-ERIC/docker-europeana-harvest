#!/bin/bash
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
FILTER_HARVEST="${SCRIPT_DIR}/filter-harvest.sh"
OUT_DIR_BASE="${SCRIPT_DIR}/workdir/preflight" 

mkdir -p "${OUT_DIR_BASE}"

if [ -d "$OUT_DIR_BASE" ]; then
	rm -rf "${OUT_DIR_BASE}"
fi

filter_collection() {
  COLLECTION="$1"
  shift
  bash "${FILTER_HARVEST}" --collection "${COLLECTION}" $@ >&2
}

gather() {
  echo '<?xml version="1.0" encoding="UTF-8"?>'
  echo '<records>'

  find "${OUT_DIR_BASE}" -name selection.xml | xargs cat | egrep '^\s*<record.*</record>'

  echo '</records>'
}

export filter_collection

if ! [ "${PREFLIGHT_DEFINITION_SCRIPT}" ]; then
	echo "Error: variable PREFLIGHT_DEFINITION_SCRIPT not set" >&2
	exit 1
fi

if [ -e "${PREFLIGHT_DEFINITION_SCRIPT}" ]; then
	source "${PREFLIGHT_DEFINITION_SCRIPT}"
else
	echo "Error: preflight definition script configured at "\
	"'${PREFLIGHT_DEFINITION_SCRIPT}'" \
	"but does not exist" >&2
	exit 1
fi

filter_collections
gather

# clean up
(rm -rf "${OUT_DIR_BASE}" || echo 'Failed to clean up') >&2

exit 0
