#!/bin/bash
set -e

OAI_SETS_INCLUDE_TARGET="${OAI_SETS_INCLUDE_TARGET-/app/oai/resources/config-europeana-europeana.xml}"
OAI_INCLUDE_PLACEHOLDER="${OAI_INCLUDE_PLACEHOLDER:-{{{SETS_LIST\}\}\}}"

if ! [ "${OAI_SETS_INCLUDE_FILE}" ]; then
	echo "Error: OAI_SETS_INCLUDE_FILE not set"
	exit 1
fi

TARGET_COPY="${OAI_SETS_INCLUDE_TARGET}.copy"
cp "${OAI_SETS_INCLUDE_TARGET}" "${TARGET_COPY}"

sed -e 's/'"${OAI_INCLUDE_PLACEHOLDER}"'/{{.env.SETS_LIST}}/g' "${TARGET_COPY}" \
	| SETS_LIST="$(<${OAI_SETS_INCLUDE_FILE})" renderizer > "${OAI_SETS_INCLUDE_TARGET}"
