#!/bin/bash
BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

if ! [ "${CMDI_RECORDS_BASE_URL}" ]; then
	echo "Error, CMDI_RECORDS_BASE_URL is not set"
	exit 1
fi

(
	cd /app/workdir/output
	CMDI_DIR="$(find . -name cmdi -type d)"
	if ! [ "${CMDI_DIR}" ]; then
		echo "Error, no 'cmdi' directory in harvester output"
		exit 1
	fi
	
	find "${CMDI_DIR}" -type f -name '*.xml' -exec \
		python3 "${BASE_DIR}/set-selflink.py" "{}" "${CMDI_RECORDS_BASE_URL}" \;
)
