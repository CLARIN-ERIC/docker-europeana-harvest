import re
import sys

from lxml import etree

HEADER_XPATH = '/cmd:CMD/cmd:Header'
SELFLINK_XPATH = '/cmd:CMD/cmd:Header/cmd:MdSelfLink'
NAMESPACES = {'cmd': 'http://www.clarin.eu/cmd/1'}

def main():
	if len(sys.argv) < 2:
		print("Error: must provide file name and base URL as arguments")
		exit(1)
	
	xml_file = sys.argv[1]
	base_url = sys.argv[2]
	
	url = f"{base_url}/{re.sub(r'^./', '', xml_file)}"
	
	doc = etree.parse(xml_file)
	selflink_nodes = doc.xpath(SELFLINK_XPATH, namespaces=NAMESPACES)
	if len(selflink_nodes) > 0:
		selflink_node = selflink_nodes[0]
	else:
		header_nodes = doc.xpath(HEADER_XPATH, namespaces=NAMESPACES)
		# no header? exit 1
		if len(header_nodes) != 1:
			print(f"Error: unexpected number of cmd:Header nodes ({len(header_node)})")
			exit(1)
		header_node = header_nodes[0]
		selflink_node = etree.SubElement(header_node, '{http://www.clarin.eu/cmd/1}MdSelfLink', nsmap=NAMESPACES)
	
	selflink_node.text = url
	
	docstr = etree.tostring(doc, xml_declaration=True, pretty_print=False, encoding='UTF-8')
	with open(xml_file, 'wb') as out_file:
		out_file.write(docstr)

	print(f'Updated self link header in {xml_file}')
		

if __name__ == "__main__":
	 main()
